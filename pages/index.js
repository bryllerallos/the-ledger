import Head from 'next/head'
import styles from '../styles/Home.module.css'
import * as ImIcons from 'react-icons/im';
import UserContext from '../UserContext'
import Router from 'next/router'
import React, { useEffect, useContext } from 'react';
import { userInfo } from 'os';



export default function Home() {

  const {user} = useContext(UserContext)
  
  useEffect(() => {
    if(user.id !== null){
      Router.push('/home')
    }else if(user.id === null){ 
      Router.push('/login')
    }else{
      Router.push('/home')
    }
  },[user.id])

  return (
    <div className={styles.container}>
      <Head>
        <title>The Ledger</title>
        <link rel="icon" href="/favicon.ico"/>
      </Head>
     
      
   </div>
  )
}
