import '../styles/globals.css'
import 'bootswatch/dist/cyborg/bootstrap.min.css'
import Navbar from '../components/Navbar'
import '../components/Navbar.css';
import { UserProvider } from '../UserContext'
import {useState, useEffect} from 'react';
import Router from 'next/router'
import Head from 'next/head';
import AppHelper from '../app-helper';

function MyApp({ Component, pageProps }) {

  const [user, setUser] = useState({
    id: null
  })

  
  const [balance, setBalance] = useState(0)
  const [records, setRecords] = useState([])
  const [income, setIncome] = useState(0)
  const [expense, setExpense] = useState(0)


  const unsetUser = () => {
    localStorage.clear()

    setUser({
      email: null
    })
    setBalance(0)
    setRecords([])
    setIncome(0)
    setExpense(0)

    Router.push('/login')
  }

  useEffect(()=> {
    fetch(`${AppHelper.API_URL}user/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('googleToken')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data, 'nice')
      if(data._id){

        const transactions = data.transactions
        const totalIncome = transactions.filter(record => record.category === 'Income')
        const totalExpense = transactions.filter(record => record.category === 'Expense')
        const totalIncomeAmount = totalIncome.map((total => total.amount))
        const sumIncome = totalIncomeAmount.reduce((total, totaIncomeAmount) =>  total + totaIncomeAmount, 0);  
        const totalExpenseAmount = totalExpense.map(total => total.amount)
        const sumExpense = totalExpenseAmount.reduce((total, totalExpenseAmount) =>  total + totalExpenseAmount, 0); 

        
        const computeBalance = () => {
          const records = data.transactions
         
          return records.reduce((accumulatedValue, currentRecord) => {
            
           
            if(currentRecord.category === 'Expense'){
              
              return +accumulatedValue - +currentRecord.amount
            }else {
      
              return +accumulatedValue + +currentRecord.amount
              
            }
          }, 0)
          
        }

        setUser({
          id: data._id,
          firstName: data.firstName.charAt(0).toUpperCase() + data.firstName.slice(1),
          lastName: data.lastName,
          email: data.email,
          loginType: data.loginType
        })

        setBalance(computeBalance())
        setRecords(data.transactions)
        setIncome(sumIncome)
        setExpense(sumExpense)
  
      }else{
        setUser({
          id: null
        })
        setBalance(0)
        setRecords([])
        setIncome(0)
        setExpense(0)
      }
    })
  }, [user.id])



  return (
    
    <>
     
     <UserProvider value={{user, setUser, unsetUser, balance, setBalance, records, setRecords, income, setIncome, expense, setExpense}}>
          <Head>
              <title>The Ledger</title>
              <link rel="icon" href="/favicon.ico"/>
          </Head>
        <Navbar/>
        <Component {...pageProps} />
     </UserProvider>
      
    </>
  )
  
  
}

export default MyApp
