import Head from 'next/head';
import {useState, useContext, useEffect} from 'react';
import UserContext from '../../UserContext';
import Router from 'next/router';
import { Form, Button, Card, Modal} from 'react-bootstrap';
import Loader from 'react-loader-spinner'
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import Pagination from '../../components/Pagination';
import AppHelper from '../../app-helper';
import * as GoIcons from 'react-icons/go';
import * as RiIcons from 'react-icons/ri';
import Swal from 'sweetalert2';




export default function index () {

    const { user, balance, setIncome, setExpense, setBalance, records, setRecords, income, expense } = useContext(UserContext);

    const userId = user.id

    const [copyRecords, setCopyRecords] = useState([])

    const Toast = Swal.mixin({
        toast: true,
        position: 'bottom-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
        })

        
    

   const firstName = user.firstName
    
    const addTransaction = () => {

        Router.push('/addRecord')
    }

    const [sortedClicked, setSortedClicked] = useState(false)

    useEffect(()=> {

    }, [sortedClicked])


    const nameSort = () => {
       records.sort(function(a,b){
        if(a.name.toLowerCase() > b.name.toLowerCase()){
            return 1;
        }else{
            return -1;
        }
        })
        
       setSortedClicked(!sortedClicked)
       

    }

    const amountSort = () => {
        records.sort(function(a,b){
            if(a.amount > b.amount){
                return 1;
            }else{
                return -1;
            }
        })
        setSortedClicked(!sortedClicked)
       
       
    }

    const dateSort = () => {
        records.sort(function(a,b){
            if(a.updatedAt > b.updatedAt){
                return 1;
            }else{
                return -1;
            }
        })
        setSortedClicked(!sortedClicked)
      
        
    }


    useEffect(() => {
        if(records.length !== 0 && copyRecords.length === 0){
            setCopyRecords([...records])
        }
   
    },[records])

    const incomeFilter = () => {
        let copyRecords1 = [...copyRecords]

        const incFilter = copyRecords1.filter(function(record){
            if(record.category === 'Income'){
                return record
            }
        })
        
        
        setRecords(incFilter)
    }


    const expenseFilter = () => {
        let copyRecords1 = [...copyRecords]

    const expFilter = copyRecords1.filter((record) => record.category === 'Expense')
       
        setRecords(expFilter)
   
    }

    const showAll = () => {

        setRecords(copyRecords)

    }

    const [input, setInput] = useState('')

    const search = e => {
        setInput(e.target.value)
        let searchRecords = [...copyRecords]
        let regex = new RegExp(`${e.target.value}\\w*`, 'ig')
        const newRecord = searchRecords.filter((record) => {
        
        return record.name.match(regex) || record.description.match(regex) 
    })

        
        setRecords(newRecord)
    }

    let copyRecords2 = [...copyRecords]


   const [currentPage, setCurrentPage] = useState(1);
   const [recordsPerPage, setRecordsPerPage] = useState(10);

   const setFive = () => setRecordsPerPage(5);
   const setTen = () => setRecordsPerPage(10);

   const lastRecordIndex = currentPage * recordsPerPage;
   const firstRecordIndex = lastRecordIndex - recordsPerPage;
    const currentRecords = records.slice(firstRecordIndex, lastRecordIndex);

    const paginate = (pageNumber) => setCurrentPage(pageNumber);

    const [showModal, setShowModal] = useState(false)
    
    const handleClose = () => setShowModal(false)

    const [currentName, setCurrentName] = useState('')
    const [currentCategory, setCurrentCategory] = useState('');
    const [currentAmount, setCurrentAmount] = useState('0');
    const [currentDescription, setCurrentDescription] = useState('');
    const [currentId, setCurrentId] = useState(null)

    useEffect(() => {
        fetch(`${AppHelper.API_URL}user/details`, {
            headers: {
              Authorization: `Bearer ${localStorage.getItem('googleToken')}`
            }
          })
          .then(res => res.json())
          .then(data => {
            

            const transactions = data.transactions
            const totalIncome = transactions.filter(record => record.category === 'Income')
            const totalExpense = transactions.filter(record => record.category === 'Expense')
            const totalIncomeAmount = totalIncome.map((total => total.amount))
            const sumIncome = totalIncomeAmount.reduce((total, totaIncomeAmount) =>  total + totaIncomeAmount, 0);  
            const totalExpenseAmount = totalExpense.map(total => total.amount)
            const sumExpense = totalExpenseAmount.reduce((total, totalExpenseAmount) =>  total + totalExpenseAmount, 0); 
    
            
            const computeBalance = () => {
              const records = data.transactions
             
              return records.reduce((accumulatedValue, currentRecord) => {
                
               
                if(currentRecord.category === 'Expense'){
                  
                  return +accumulatedValue - +currentRecord.amount
                }else {
          
                  return +accumulatedValue + +currentRecord.amount
                  
                }
              }, 0)
              
            }
            setBalance(computeBalance())
            setRecords(transactions)
            setIncome(sumIncome)
            setExpense(sumExpense)
          })   
   
    },[currentAmount, currentName, currentDescription, currentCategory, records])

    

    const userRecords = currentRecords.map(record => {

        
        const copyAmount = record.amount

        const handleModal = () => {
            
            setShowModal(true)
           
            setCurrentName(record.name)
            setCurrentCategory(record.category)
            setCurrentAmount(record.amount)
            setCurrentDescription(record.description)
            setCurrentId(record._id)
           
        }

        const handleUpdate = (e) => {

            e.preventDefault()

            const singleEntry = () => {
                if(copyAmount > currentAmount){
                   return copyAmount
                }else{
                   return currentAmount
                }
            }


            const sendBalToServer = singleEntry()

            console.log(sendBalToServer)
            
            
            const updateBalance = () => {

                if(currentCategory === 'Expense'){
                    if(copyAmount === currentAmount){
                        return +balance  - parseInt(currentAmount) - parseInt(copyAmount)
                    }else{
                        return +balance - parseInt(copyAmount) - parseInt(currentAmount)
                    }
                }else if(currentCategory === 'Income'){
                    if(copyAmount === currentAmount){
                        return +balance + parseInt(currentAmount) + parseInt(copyAmount)
                    }else{
                        return +balance + parseInt(currentAmount) - parseInt(copyAmount)
                    }
                }
        
            }
            

            const balanceToServer = updateBalance()

           

            const complete = (currentName !== '' && currentCategory !== '' && currentDescription !== '' && currentAmount !== '') && (currentAmount > 0 || currentAmount.length > 9) 
            
            if(complete){
                

                fetch(`${AppHelper.API_URL}record/${currentId}`, {
                    method: 'PATCH',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${localStorage.getItem('googleToken')}`
                    },
                    body: JSON.stringify({
                        name: currentName,
                        category: currentCategory,
                        description: currentDescription,
                        amount: currentAmount
                    
                    })
                })
                .then(res =>  {
                    return res.json()
                })
                .then(data => {
                    
                    if(data === false){
                        Swal.fire('Error', 'Please Check the data', 'error')
                    }else{
                        if(currentRecords.length === 1){
                            fetch(`${AppHelper.API_URL}user/${userId}`, {
                                method: 'PATCH',
                                headers: {
                                    'Content-Type': 'application/json',
                                    Authorization: `Bearer ${localStorage.getItem('googleToken')}`
                                },
                                body: JSON.stringify({
                                    balance: sendBalToServer
                                })
                            })
                            .then(res => res.json())
                            .then(data => {
                            console.log(data, 'test')
                            if(data === false){
                                Swal.fire('Error', 'Please Check the data', 'error')
                            }else{
                                setBalance(data.balance)
                              
                                setRecords(data.transactions)
                                Toast.fire({
                                    icon: 'success',
                                    title: 'Record Updated Successfully'
                                    })
                                const transactions = data.transactions
                                const totalIncome = transactions.filter(record => record.category === 'Income')
                                const totalExpense = transactions.filter(record => record.category === 'Expense')
                                const totalIncomeAmount = totalIncome.map(total => total.amount)
                                const sumIncome = totalIncomeAmount.reduce((total, totaIncomeAmount) =>  total + totaIncomeAmount, 0);  
                                const totalExpenseAmount = totalExpense.map(total => total.amount)
                                const sumExpense = totalExpenseAmount.reduce((total, totalExpenseAmount) =>  total + totalExpenseAmount, 0); 
                                setIncome(sumIncome)
                                setExpense(sumExpense)
                                
                            }
    
                                
                        }) 
                        }else{
                            fetch(`${AppHelper.API_URL}user/${userId}`, {
                                method: 'PATCH',
                                headers: {
                                    'Content-Type': 'application/json',
                                    Authorization: `Bearer ${localStorage.getItem('googleToken')}`
                                },
                                body: JSON.stringify({
                                    balance: balanceToServer
                                })
                            })
                            .then(res => res.json())
                            .then(data => {
                                console.log(data, 'test')
                                if(data === false){
                                    Swal.fire('Error', 'Please Check the data', 'error')
                                }else{
                                    setBalance(data.balance)
                                  
                                    setRecords(data.transactions)
                                    Toast.fire({
                                        icon: 'success',
                                        title: 'Record Updated Successfully'
                                        })
                                    const transactions = data.transactions
                                    const totalIncome = transactions.filter(record => record.category === 'Income')
                                    const totalExpense = transactions.filter(record => record.category === 'Expense')
                                    const totalIncomeAmount = totalIncome.map(total => total.amount)
                                    const sumIncome = totalIncomeAmount.reduce((total, totaIncomeAmount) =>  total + totaIncomeAmount, 0);  
                                    const totalExpenseAmount = totalExpense.map(total => total.amount)
                                    const sumExpense = totalExpenseAmount.reduce((total, totalExpenseAmount) =>  total + totalExpenseAmount, 0); 
                                    setIncome(sumIncome)
                                    setExpense(sumExpense)
                                    
                                }
        
                                    
                            })

                        }
    
                                handleClose()
                               
                    }
                })    

            }else{
                Swal.fire({
                    title: 'Uh-Oh',
                    text: 'Please Complete all the Fields',
                    icon: 'error'
                })
            }

        }



        const date = record.updatedAt
     
        const postDate = new Date(date)
        
        const updatedDate = postDate.toString()
 
        const year = postDate.getFullYear()
        const month = postDate.getMonth()+1
        const day =  postDate.getDate()
        const hour = postDate.getHours() > 12 ? postDate.getHours() - 12 : postDate.getHours() 
        const min = postDate.getMinutes() > 10 ? postDate.getMinutes() : '0' + postDate.getMinutes()

        const newDate = updatedDate.slice(0,21)

        const format12 = postDate.getHours() > 11 ? 'PM' : 'AM'

        const amount = record.amount.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
        
        const name = record.name
        

        function nameFormat(name){
            return name.charAt(0).toUpperCase() + name.slice(1)
        }
        const nameUpperCase = name.split(' ').map(nameFormat).join(' ')

        
        
        const description = record.description

        function descFormat(description){
            return description.charAt(0).toUpperCase() + description.slice(1)
        }

        const descUpperCase = description.split(' ').map(descFormat).join(' ')

        const handleDelete = () => {

            const newBalance = balance - record.amount
                
                fetch(`${AppHelper.API_URL}user/${userId}`, {
                    method: 'PATCH',
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${localStorage.getItem('googleToken')}`
                    },
                    body: JSON.stringify({
                        balance: newBalance
                    })
                })
                .then(res => res.json())
                .then(data => {
                    if(data){

                        fetch(`${AppHelper.API_URL}record/${record._id}`, {
                            method: 'delete',
                            headers: {
                                Authorization: `Bearer ${localStorage.getItem('googleToken')}`
                         }
                        })
                        .then(res => res.json())
                        .then(data => {
                           
                        })

                        Toast.fire({
                            icon: 'success',
                            title: 'Record Deleted Successfully'
                            })
                    }else{
                        Swal.fire({
                            icon: 'error',
                            title: 'Process Failed',
                            text: 'Please Try Again Later',
                            timer: 1000,
                            showCloseButton: false
                        })
                    }

               
            })

            
        }

        

        
        return (
            <>
                <tr key = {record._id}>
                
                   
                    {record.category === 'Expense' 
                    ?
                    <>
                    <td><strong className='text-warning'>{updatedDate.slice(0,16) + hour +  ':' + min + '' + format12 }</strong></td>
                    <td><strong className='text-warning'>{nameUpperCase}</strong></td>
                    <td><strong className='text-warning'>{record.category}</strong></td>
                    <td><strong className='text-warning'>{descUpperCase}</strong></td>
                    <td><strong className='text-warning'>( ₱ {amount} )</strong></td>
                    <td>
                        <div className='editIcon'>
                            <GoIcons.GoPencil size={20} style={{fill:'#FFA900'}} onClick={handleModal}/>
                            <RiIcons.RiDeleteBin7Fill size={20} style={{fill:'#ffdde1', margin: '0 5px'}} onClick={handleDelete}/>
                            
                        </div>
                            
                    </td>
                    </>
                    :
                    <>
                    <td><strong>{updatedDate.slice(0,16) + hour +  ':' + min + '' + format12 }</strong></td>
                    <td><strong>{nameUpperCase}</strong></td>
                    <td><strong>{record.category}</strong></td>
                    <td><strong>{descUpperCase}</strong></td>
                    <td><strong>₱ {amount}</strong></td>
                    <td>
                        
                        <div className='editIcon'>
                            <GoIcons.GoPencil size={20} style={{fill:'#FFA900'}} onClick={handleModal}/>
                            <RiIcons.RiDeleteBin7Fill size={20} style={{fill:'#ffdde1', margin: '0 5px'}} onClick={handleDelete}/>
                            
                        </div>
                   
                        
                    </td>
                    
                    </>
                    
                    } 
                   
                </tr>

                <Modal show={showModal}
                    onHide={handleClose}
                    size="sm"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered
                >
                    <Modal.Header closeButton>
                        <Modal.Title className='text-center'>Update Record</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                    <Form autoComplete='off'>
                                        <Form.Group controlId='name'>
                                            <Form.Label>Name</Form.Label>
                                            <Form.Control className='badge-pill' type='text' value={currentName} onChange={e => setCurrentName(e.target.value)} required></Form.Control>
                                        </Form.Group>
                                        <Form.Group controlId='category'>
                                            <Form.Label>Category</Form.Label>
                                            <Form.Control className='badge-pill' as='select' value={currentCategory} onChange={e => setCurrentCategory(e.target.value)} required>
                                                <option className='badge-pill'></option>
                                                <option className='badge-pill' value="Income">Income</option>
                                                <option className='badge-pill' value="Expense">Expense</option>
                                            </Form.Control>
                                        </Form.Group>
                                        <Form.Group controlId='description'>
                                            <Form.Label>Description</Form.Label>
                                            <Form.Control className='badge-pill' type='text' value={currentDescription}  onChange={e => setCurrentDescription(e.target.value)} required></Form.Control>
                                        </Form.Group>
                                        <Form.Group controlId='amount'>
                                            <Form.Label>Amount</Form.Label>
                                            <Form.Control className='badge-pill' type='number' value={currentAmount}  onChange={e => setCurrentAmount(e.target.value)}></Form.Control>
                                        </Form.Group>
                                        <Button className='badge-pill btn-block btn-outline btn-warning' onClick={handleUpdate} >
                                            Update
                                        </Button>
                                        <Button className='badge-pill btn-block btn-outline btn-info' onClick={handleClose}>
                                            Cancel
                                        </Button>
                                        
                                    </Form>
                    </Modal.Body>
                </Modal>
            </>
        )
    })
 

    return(
        <div className="container" id='recordContainer'>
             <Head>
                <title>The Ledger - Home</title>
                <link rel="icon" href="/favicon.ico"/>
            </Head>
            
         
                <div className="row text-center">
                    <div className="col-md-8 offset-md-2">
                       <div className="card text-center">
                           <div className="card-header">
                               <h4>Hi <em>{firstName}</em> ! Welcome Back</h4>
                           </div>
                       </div>
                      
                    </div>
                </div>
                
                <div className="row text-center">
                    <div className="col-md-8 offset-md-2">
                        <div className="card card-bordered">
                            {balance <= 0 ? 
                                <>
                                <h3 className='text-center text-warning card-header'>Current Balance: (₱ {balance.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')})</h3>
                                </>
                                :
                                <>
                                <h3 className='text-center card-header'>Current Balance: ₱ {balance.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}</h3>
                                </>
                            }
                            
                            <div className="row">
                                <div className="col-md-6 d-block text-center">
                                    <h5 className='text-center'>Total Income</h5>
                                    <h5> ₱ {income.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}</h5>
                                </div>
                                <div className="col-md-6 d-block text-center text-warning" >
                                    <h5 className='text-center'>Total Expense</h5>
                                    <h5> ₱ {expense.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>     
                    <div className="row d-flex">
                        <div className="col-md-9">
                           
                        
                            <h3 id='title'>Transactions</h3>
                            <button className='buttons m-3 badge-pill btn-outline-primary bg-light' onClick={addTransaction}>Add Transaction</button>
                                     <Loader 
                                        type="ThreeDots"
                                        color="#ffdde1"
                                        height={200}
                                        width={300}
                                        timeout={800}
                                        
                                        />
                            <table className="table-bordered table-striped table-lg col-lg-12">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Name</th>
                                            <th>Type</th>
                                            <th>Description</th>
                                            <th>Amount</th>
                                            <th>Action</th>
                                        
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {userRecords}
                                       
                                        
                                    </tbody>
                                           
                                </table>
                                
                                    <Pagination recordsPerPage={recordsPerPage} totalRecords ={records.length} paginate={paginate}/>
                                
                                <div className='d-flex'>
                                   <p>Items Per Page
                                     <button className='buttons mx-2 badge-pill btn-outline-primary bg-light' type='button' onClick={setFive}>5</button>
                                     <button className='buttons badge-pill btn-outline-primary bg-light' type='button' onClick={setTen}>10</button>
                                   </p>
                                   
                                </div>
                                
                            </div>
                            
                            <div className="col-md-3 py-3">
                                    <div className="pt-4">
                                        <Card className='mt-3'>
                                            <Card.Header>Sort Data</Card.Header>
                                            <Card.Body className='d-block'>
                                                    <Button className='mr-1 badge-pill px-2 btn-outline-primary bg-light' type='button' onClick={() => nameSort()}> Name</Button>
                                                    <Button className='mx-1 badge-pill px-1 btn-outline-primary bg-light' type='button' onClick={() => amountSort()}> Amount</Button>
                                                    <Button className='ml-1 px-3 badge-pill btn-outline-primary bg-light' type='button' onClick={() => dateSort()}> Date</Button>
                                            </Card.Body>
                                        </Card>
                                    </div>
                                   

                                    <Card className='my-1'>
                                        <Card.Header>Filter Data</Card.Header>
                                        <Card.Body className='d-block'>
                                                <Button className='m-1 badge-pill px-2 btn-outline-primary bg-light' type='button' onClick={() => incomeFilter()}> Income</Button>
                                                <Button className='m-1 badge-pill px-2 btn-outline-primary bg-light' type='button' onClick={() => expenseFilter()}> Expense</Button>
                                                <Button className='m-1 badge-pill px-2 btn-outline-primary bg-light' type='button' onClick={() => showAll()}> Show All</Button>
                                                
                                        </Card.Body>
                                    </Card>

                                    <Card className='my-1'>
                                        <Form>
                                            <Form.Group>
                                                <Form.Label>Search Transaction</Form.Label>
                                                <Form.Control placeholder='Enter Name or Description' value={input} onChange={ e => search(e) } />
                                            </Form.Group>
                                        </Form>
                                    </Card>
                                
                                            
                            </div>               
                       
                    </div>
               
                           
                 
                                    
        </div> 
    )
}