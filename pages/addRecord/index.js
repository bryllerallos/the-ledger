import Head from 'next/head';
import {useState, useContext} from 'react';
import UserContext from '../../UserContext';
import { Card, Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import Router from 'next/router';
import AppHelper from '../../app-helper';
import LottieAnimation from '../../components/Lottie';
import home from '../../components/Login Animation/Lottie Animation/Blogging Colored.json'


export default function index () {

    const { user, balance, setBalance, setRecords, setIncome, setExpense} = useContext(UserContext);

    const [name, setName] = useState('')
    const [category, setCategory] = useState('');
    const [amount, setAmount] = useState('0');
    const [description, setDescription] = useState('');
    

    const addTransaction = (e) => {
        e.preventDefault()
        const complete = (name !== '' && category !== '' && amount !== '' && description !== '') && (amount > 0) && (amount.length < 10)

        if(complete){
            const newBalance = () => {
                if(category === 'Expense'){
                    return +balance - +amount
                }else{
                    return +balance + +amount
                }
            }
            const balanceToServer = newBalance()
            
                    fetch(`${AppHelper.API_URL}record/add`, {
                        method: 'post',
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': `Bearer ${localStorage.getItem('googleToken')}`
                        },
                        body: JSON.stringify({
                            name: name,
                            category: category,
                            description: description,
                            amount: amount
                        })
                    })
                    .then(res => {
                        return res.json()
                    })
                    .then(data => {
                        if(data === false){
                            Swal.fire('Error', 'Please Check the data', 'error')
                        }else{
                            fetch(`${AppHelper.API_URL}user/${data}`, {
                                method: 'PATCH',
                                headers: {
                                    'Content-Type': 'application/json',
                                    Authorization: `Bearer ${localStorage.getItem('googleToken')}`
                                },
                                body: JSON.stringify({
                                    balance: balanceToServer
                                })
                            })
                            .then(res => res.json())
                            .then(data => {
                                
                                if(data === false){
                                    Swal.fire('Error', 'Please Check the data', 'error')
                                }else{
                                    setBalance(data.balance)
                                  
                                    setRecords(data.transactions)
                                    const Toast = Swal.mixin({
                                        toast: true,
                                        position: 'bottom-end',
                                        showConfirmButton: false,
                                        timer: 3000,
                                        timerProgressBar: true,
                                        didOpen: (toast) => {
                                            toast.addEventListener('mouseenter', Swal.stopTimer)
                                            toast.addEventListener('mouseleave', Swal.resumeTimer)
                                        }
                                        })

                                        Toast.fire({
                                        icon: 'success',
                                        title: 'Record Added Successfully'
                                        })
                                    const transactions = data.transactions
                                    const totalIncome = transactions.filter(record => record.category === 'Income')
                                    const totalExpense = transactions.filter(record => record.category === 'Expense')
                                    const totalIncomeAmount = totalIncome.map(total => total.amount)
                                    const sumIncome = totalIncomeAmount.reduce((total, totaIncomeAmount) =>  total + totaIncomeAmount, 0);  
                                    const totalExpenseAmount = totalExpense.map(total => total.amount)
                                    const sumExpense = totalExpenseAmount.reduce((total, totalExpenseAmount) =>  total + totalExpenseAmount, 0); 
                                    setIncome(sumIncome)
                                    setExpense(sumExpense)
                                    Router.push('/home')
                                }
                            })
                        }
                    })    
        }else{
            const Toast = Swal.mixin({
                toast: true,
                position: 'bottom-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
             })

              Toast.fire({
               icon: 'error',
               title: 'Please Try Again Later'
             })
        }

        
        
    }
    

    return(
        <>
             <Head>
                <title>Ledger - Add Transaction</title>
                <link rel="icon" href="/favicon.ico"/>
            </Head>
            
            <div className="container my-5">
                <div className="row my-5">
                    <div className="col-lg-12">
                        <div className="card">
                            <h5 className='text-center my-2'>Create A Transaction</h5>
                            <div className="row d-flex">
                                <div className="col-md-7 text-center">
                                    <LottieAnimation lotti={home} height={280} width={280}/>
                                    <ul className='list-unstyled'>
                                        <li>Complete all the fields</li>
                                        <li>Amount should not exceed 9 characters</li>
                                    </ul>
                                </div> 
                                <div className="col-md-4 my-3">
                                    <Form onSubmit ={addTransaction} autoComplete='off'>
                                        <Form.Group controlId='name'>
                                            <Form.Label>Name</Form.Label>
                                            <Form.Control className='badge-pill' type='text' value={name} onChange={e => setName(e.target.value)} required></Form.Control>
                                        </Form.Group>
                                        <Form.Group controlId='category'>
                                            <Form.Label>Category</Form.Label>
                                            <Form.Control className='badge-pill' as='select' value={category} onChange={e => setCategory(e.target.value)} required>
                                                <option className='badge-pill' value=""></option>
                                                <option className='badge-pill' value="Income">Income</option>
                                                <option className='badge-pill' value="Expense">Expense</option>
                                            </Form.Control>
                                        </Form.Group>
                                        <Form.Group controlId='description'>
                                            <Form.Label>Description</Form.Label>
                                            <Form.Control className='badge-pill' type='text' value={description} onChange={e => setDescription(e.target.value)} required></Form.Control>
                                        </Form.Group>
                                        <Form.Group controlId='amount'>
                                            <Form.Label>Amount</Form.Label>
                                            <Form.Control className='badge-pill' type='number' value={amount} onChange={e => setAmount(e.target.value)} required></Form.Control>
                                        </Form.Group>
                                        <Button className='badge-pill btn-block btn-outline btn-warning' type='submit'>Create</Button>
                                        
                                    </Form>
                                </div>    
                            </div>
                            
                        </div>
                       
                 
                    </div>
                
                </div>
            </div>
            
            
        
        </>
    )
}