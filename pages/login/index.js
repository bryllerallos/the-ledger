import { useState, useContext } from 'react';
import Head from 'next/head';
import { Form, Button, Row, Col, Card } from 'react-bootstrap';
import UserContext from '../../UserContext';
import Router from 'next/router';
import View from '../../components/View';
import { GoogleLogin } from 'react-google-login';
import AppHelper from '../../app-helper';
import Swal from 'sweetalert2';
import LottieAnimation from '../../components/Lottie';
import home from '../../components/Login Animation/Lottie Animation/Login Colored.json'
import Link from 'next/head'


export default () => {
    return (
        <View title={ 'Login' }>
            <Row className="justify-content-center">
                <Col xs md="12">
                    
                    <LoginForm/>
                </Col>
            </Row>
        </View>
    )
}
const LoginForm = () => {
	
	const {user, setUser} = useContext(UserContext)
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [tokenId, setTokenId] = useState(null);
	
	const authenticateGoogleToken = (response) => {
		
		const payload = {
			method: 'post',
			headers: {'Content-type': 'application/json'},
			body: JSON.stringify({tokenId: response.tokenId})
		}

		fetch(`${AppHelper.API_URL}user/verify-google-id-token`, payload).then(AppHelper.toJSON)
		.then(data => {
           
			if(typeof data.accessToken !== 'undefined'){
				localStorage.setItem('googleToken', data.accessToken)
				retrieveUserDetails(data.accessToken)
				
			}else{
					if (data.error == 'google-auth-error'){
						Swal.fire(
							'Google Authentication Error',
							'Google authentication procedure failed',
							'error'
						)
					} else if (data.error === 'login-type-error') {
						Swal.fire(
							'Login Type Error',
							'You may have registered through a different login procedure',
								'error'
						)
					}
		}
	})
}

	const retrieveUserDetails = (accessToken) => {
		const option = {
			headers: { Authorization: `Bearer ${accessToken}` } 
		}

	


		fetch(`${AppHelper.API_URL}user/details`, option).then(AppHelper.toJSON).then(data => {
			

			setUser(
				{ id: data._id,
				  email: data.email,
				  firstName: data.firstName,
				  lastName: data.lastName,
				  transactions: data.transactions	
				}
				)
			Router.push('/home')
			

			
		})
	
	}


	const login = e => {
		
		e.preventDefault()
		const details = {
			method: 'post',
			headers: {'Content-type': 'application/json'},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}

		fetch(`${AppHelper.API_URL}user/login`, details)
		.then(AppHelper.toJSON)
		.then(data => {
			console.log(data, 'test')
			if(data === false){
				Swal.fire({
					icon: 'warning',
					title: 'Invalid Credentials',
					text: 'Please Check login details'
				})
			}else{
				localStorage.setItem('googleToken', data.accessToken)
				if(data.accessToken){
				
				
				const token = data.accessToken	
						const access = {
						method: 'get',
						headers: {Authorization: `Bearer ${token}`}
					}

					fetch(`${AppHelper.API_URL}user/details`, access)
					.then(AppHelper.toJSON)
					.then(data => {
						console.log(data)
						setUser(
							{ id: data._id,
							  email: data.email,
							  firstName: data.firstName,
							  lastName: data.lastName,
							  transactions: data.transactions,	
							  loginType: data.loginType
							}
							)
						
						Router.push('/home')
						
					})
				}
			}
			
		})

		

		
	}
	
	const authenticate = e => {
		e.preventDefault()

		const options = {
			method: 'post',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({email: email, password: password})
		}

				fetch(`${AppHelper.API_URL}user/login`, options).then(AppHelper.toJSON).then(data => {
					if(typeof data.accessToken !== 'undefined'){
						localStorage.setItem('token', data.accessToken)
						retrieveUserDetails(data.accessToken)
						
					}else{
						if(data.error === 'does not exist'){
							Swal.fire(
								'Authentication failed', 
								'User undefined', 
								'error'
							)
						}else if(data.error === 'incorrect password'){
							Swal.fire(
								'Authentication failed',
								'Incorrect Password',
								'error'
							)
						}else if(data.error === 'login-type-error') {
							Swal.fire(
								'Login-type Error',
								'Try using an alternative login procedure',
								'error'
							)
						}
					}
				})
	}

	return(
	<>
		<Head>
                <title>The Ledger - Login</title>
                <link rel="icon" href="/favicon.ico"/>
        </Head>
		<div className="container">
			<div className="row d-flex">
				<div className="col-md-12">
					<div className="card d-flex">
						<h3 className='card-header bg-light text-center'>Welcome to The Ledger</h3>
						<div className="row d-flex">
							<div className="col-md-6 offset-md-1 text-center my-3">
								<LottieAnimation className='text-center' lotti={home} height={250} width={250}/>
								<p className='my-3'><em>Keep Track of your Funds</em></p>
							</div>

							<div className="col-md-4 tex-center">
								<Form autoComplete='off'>
									<h5 className='text-center my-2'>Login Details</h5>
									<Form.Group controlId='email'>
										<Form.Label>Email Address</Form.Label>
										<Form.Control className='badge-pill' type='email' value={email} onChange={e => setEmail(e.target.value)} required></Form.Control>
									</Form.Group>
									<Form.Group controlId='password'>
										<Form.Label>Password</Form.Label>
										<Form.Control className='badge-pill' type='password' value={password} onChange={e => setPassword(e.target.value)} required></Form.Control>
									</Form.Group>
									<Button onClick={login} type='click' className='bg-warning btn-block badge-pill my-2 text-center'>Login</Button>
									
										<GoogleLogin
												onSubmit={authenticate}
												clientId = "22079506678-gmsf5csh67cvkehih2cbb6o7i5g76nmd.apps.googleusercontent.com"
												render={renderProps => (
												<Button onClick={renderProps.onClick} className='badge-pill bg-info text-center my-2 btn-block'>Login with Google</Button>
												)}
												onSuccess={ authenticateGoogleToken }
												onFailure={ authenticateGoogleToken }
												cookiePolicy={ 'single_host_origin' }
												
										/>

								<h6 className='m-3'><strong>Click <a className='link' href='/register'>here</a> to Create an Account</strong></h6>
								</Form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</>	
	)

}
































