import {useState} from 'react';
import {Form, Button} from 'react-bootstrap';
import Head from 'next/head';
import Router from 'next/router';
import Swal from 'sweetalert2';
import AppHelper from '../../app-helper';
import LottieAnimation from '../../components/Lottie';
import home from '../../components/Login Animation/Lottie Animation/addRecord.json'
import * as AiIcons from 'react-icons/ai';




export default function index () {

    const [formData, setFormData] = useState({
        firstName: "",
        lastName: "",
        email: "",
        password: "",
        confirmPassword: ""
    });

    const { firstName, lastName, email, password, confirmPassword } = formData

    const onChangeHandler = e => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        })
    }

    const [reveal, setReveal] = useState(false)

    const showPassword = () => {
        setReveal(!reveal)
        console.log(reveal)
    }


    


    const complete = (firstName !== '' && lastName !== '' && email !== '' && password !== '' && confirmPassword !== '') && (confirmPassword === password)
    

    const register = e => {
        e.preventDefault()

        const option = {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        }

        if(complete){
            fetch(`${AppHelper.API_URL}user/email-exists`, option)
            .then(res => res.json())
            .then(data => {
                if(data === false){

                    const method = {
                        method: 'post',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify(formData)
                    }

                    fetch(`${AppHelper.API_URL}user/register`, method)
                    .then(res => res.json())
                    .then(data => {
                        
                        Swal.fire({

                            title: 'Registration Successful',
                            showConfirmButton: false,
                            position: 'top-end',
                            timer: 1500
                            
                        })
                        Router.push('/login')
                        
                        
                    })
                }else{
                    Swal.fire({
                    icon: 'info',
                    title: 'Email Already in Use',
                    text: 'Please use another email address',
                    showConfirmButton: false,
                    position: 'top-end',
                    timer: 1500
                    
                })
                }
            })
        }else {
            Swal.fire({
                icon: 'info',
                title: 'Registration failed',
                text: 'Please check your details',
                showConfirmButton: false,
                position: 'top-end',
                timer: 1500
            })
        }

      
    }

    return(
        <>
            
           
             <Head>
                <title>The Ledger - Register</title>
                <link rel="icon" href="/favicon.ico"/>
            </Head>
            <div className="container my-5">
                <div className="row d-flex">
                    <div className="col-md-10 offset-md-1 text-center">
                        <div className="card">
                        <h5 className='text-center my-2'>Create Account</h5>
                        <div className="row d-flex">
                            <div className="col-md-7 offset-md-1 text-center my-2">
								<LottieAnimation lotti={home} height={250} width={320}/>
								<p><em>Never Share your Login Details to anyone</em></p>
							</div>
                            <div className="col-md-3 text-center">
                                    
                                    <Form autoComplete='off' value={formData} onSubmit={register}>
                                       <Form.Group>
                                           <Form.Control className='badge-pill'  type="text" id="firstName" name="firstName" placeholder='First Name' value={firstName} onChange={e => onChangeHandler(e)}  required/>
                                       </Form.Group>
                                       <Form.Group>
                                           <Form.Control className='badge-pill'  type="text" id="lastName" name="lastName" placeholder='Last Name' value={lastName} onChange={e => onChangeHandler(e)}  required />
                                       </Form.Group>
                                       <Form.Group>
                                           <Form.Control className='badge-pill'  type="email" id="email" name="email" placeholder='Email Address' value={email} onChange={e => onChangeHandler(e)}  required />
                                       </Form.Group>
                                       <Form.Group>
                                            

                                            
                                            <div className='iconContainer'>
                                                <Form.Control className='badge-pill'  type={reveal ? 'text' : 'password'} id="password" name="password" placeholder='Password' value={password} onChange={e => onChangeHandler(e)}  required 
                                            />
                                            {reveal ?
                                            <>
                                             <AiIcons.AiFillEyeInvisible onClick={showPassword}  className='showIcon' /> 
                                             </>
                                             : 
                                             <>
                                             <AiIcons.AiFillEye onClick={showPassword}  className='showIcon' /> 
                                             </>
                                            }
                                            </div>
                                            
                                           
                                       </Form.Group>
                                       <Form.Group>
                                            <div className='iconContainer'>
                                                <Form.Control className='badge-pill'  type={reveal ? 'text' : 'password'} id="confirmPassword" name="confirmPassword" placeholder='Confirm Password' value={confirmPassword} onChange={e => onChangeHandler(e)}  required />
                                                {reveal ?
                                            <>
                                             <AiIcons.AiFillEyeInvisible onClick={showPassword} className='showIcon' /> 
                                             </>
                                             : 
                                             <>
                                             <AiIcons.AiFillEye onClick={showPassword}  className='showIcon' /> 
                                             </>
                                            }        
                                            </div>    
                                       </Form.Group>
                                        <Button type='submit' className='badge-pill btn-block btn-outline btn-warning' >Register</Button>
                        
                                    </Form>
                                    

                                    <h6 className='m-3'><strong>Click <a className='link' href='/login'>here</a> to Login</strong></h6>   
                        </div>
                          
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </> 
    )
}





