import DoughnutChart from '../../components/DoughnutChart';
import {useState, useContext, useEffect} from 'react';
import UserContext from '../../UserContext';
import LottieAnimation from '../../components/Lottie';
import home from '../../components/Growth Animation/Growth Coloured/4272-data-dashboard.json'
import Head from 'next/head';
import Loader from 'react-loader-spinner'
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"


export default function index () {

    const { user, balance, setBalance, records, setRecords, income, expense } = useContext(UserContext);

    const [display, setDisplay] = useState('')


    useEffect(()=> {
        if(balance === 0){
           setDisplay(`Break Even! No profit or loss!`)
        }else if (balance > 0){
            setDisplay('Good Job! You are still on track of your finances!')
        }else {
            setDisplay('You are Down and Out! Better check your spending behaviour!')
        }
    },[balance])

   let name = 'Analysis'

    return(
        <div className="container d-flex pt-5">
            <Head>
                <title>The Ledger - Record Analysis</title>
                <link rel="icon" href="/favicon.ico"/>
            </Head>
        
            
                
                <div className="row">
                
                    <div className="card card-light text-center">
                    <Loader 
                            type="ThreeDots"
                            color="#ffdde1"
                            height={200}
                            width={300}
                            timeout={500}
                        />
                        <div className="row d-flex">
                        
                            <div className="col-lg-6">
                                <div className="text-center pt-3">
                                    <LottieAnimation lotti={home} height={300} width={300}/>
                                    <h4 className='analysis typewriter'>{name}</h4>
                                    {balance <= 0
                                    ?
                                    <>
                                    <h5 className='text-warning my-3'>{display}</h5>
                                    </>
                                    :
                                    <>
                                    <h5 className='my-3'>{display}</h5>
                                    </>
                                    }
                                    
                                </div>
                            </div>
                            <div className="col-lg-6 d-flex">
                                <div className='my-5'>
                                    <h4 className='mt-3'>Remaining Balance <p>₱ {balance.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}</p></h4>
                                    <h5 id='income'>Total Income <p>₱ {income.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}</p></h5>
                                    <h5 id='expense'>Total Expense <p className='text-warning'>₱ {expense.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}</p></h5>
                                </div>
                                 <div className='my-5 text-center'>
                                    <h4>Income and Expense Breakdown</h4>
                                    <DoughnutChart income={income} expense={expense} />
                                 </div>  
                                
                            </div>
                        </div>
                    </div>
                </div>
            
           
        </div> 
    )
}