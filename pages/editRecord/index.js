import Head from 'next/head';
import {useState, useContext} from 'react';
import UserContext from '../../UserContext';
import { Card, Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import Router from 'next/router';


export default function index () {

    const { user, balance, setBalance, records, setRecords, setIncome, setExpense} = useContext(UserContext);

    const [name, setName] = useState('')
    const [category, setCategory] = useState('');
    const [amount, setAmount] = useState('0');
    const [description, setDescription] = useState('');
    
        
   

    // console.log(record._id, 'test')
    // console.log(balance)


    const editTransaction = (e) => {
        e.preventDefault()
        const complete = (name !== '' && category !== '' && amount !== '' && description !== '')

        if(complete){
           
                    fetch(`http://localhost:4000/record/${records._id}`, {
                        method: 'put',
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': `Bearer ${localStorage.getItem('googleToken')}`
                        },
                        body: JSON.stringify({
                            name: name,
                            category: category,
                            description: description,
                            amount: amount
                        })
                    })
                    .then(res => {
                        return res.json()
                    })
                    .then(data => {
                         const computeBalance = () => {
                            const records = data.transactions
                            console.log(records, 'transto')
                            return records.reduce((accumulatedValue, currentRecord) => {
                                console.log(currentRecord.amount, 'currentRecord')
                                console.log(accumulatedValue, 'acc')
                            
                                if(currentRecord.category === 'Expense'){
                                
                                return +accumulatedValue - +currentRecord.amount
                                }else {
                        
                                return +accumulatedValue + +currentRecord.amount
                                
                                }
                            }, 0)
                        
                        }   

                        if(data === false){
                            Swal.fire('Error', 'Please Check the data', 'error')
                        }else{
                            fetch(`http://localhost:4000/user/${data}`, {
                                method: 'PATCH',
                                headers: {
                                    'Content-Type': 'application/json',
                                    Authorization: `Bearer ${localStorage.getItem('googleToken')}`
                                },
                                body: JSON.stringify({
                                    balance: balanceToServer
                                })
                            })
                            .then(res => res.json())
                            .then(data => {
                                console.log(data, 'DATA')
                                if(data === false){
                                    Swal.fire('Error', 'Please Check the data', 'error')
                                }else{
                                    setBalance(data.balance)
                                    console.log(data.transactions, 'record')
                                    setRecords(data.transactions)
                                    Swal.fire('Success', 'New Record Added', 'success')
                                    const transactions = data.transactions
                                    const totalIncome = transactions.filter(record => record.category === 'Income')
                                    const totalExpense = transactions.filter(record => record.category === 'Expense')
                                    const totalIncomeAmount = totalIncome.map(total => total.amount)
                                    const sumIncome = totalIncomeAmount.reduce((total, totaIncomeAmount) =>  total + totaIncomeAmount, 0);  
                                    const totalExpenseAmount = totalExpense.map(total => total.amount)
                                    const sumExpense = totalExpenseAmount.reduce((total, totalExpenseAmount) =>  total + totalExpenseAmount, 0); 
                                    setIncome(sumIncome)
                                    setExpense(sumExpense)
                                    Router.push('/home')
                                }
                            })
                        }
                    })    
        }

        
        
    }
    

    return(
        <>
            <Head>
                <title>Edit Record</title>
            </Head>
            
            <div className="container">
                <div className="row">
                    <div className="col-lg-12 align-items-center justify-content-center">
                        <Card>
                            <Card.Header>Edit Transaction</Card.Header>
                            <Card.Body>
                                <Form onSubmit ={editTransaction} autoComplete='off'>
                                    <Form.Group controlId='name'>
                                        <Form.Label>Name</Form.Label>
                                        <Form.Control type='text' value={name} onChange={e => setName(e.target.value)} required></Form.Control>
                                    </Form.Group>
                                    <Form.Group controlId='category'>
                                        <Form.Label>Category</Form.Label>
                                        <Form.Control as='select' value={category} onChange={e => setCategory(e.target.value)} required>
                                            <option value=""></option>
                                            <option value="Income">Income</option>
                                            <option value="Expense">Expense</option>
                                        </Form.Control>
                                    </Form.Group>
                                    <Form.Group controlId='description'>
                                        <Form.Label>Description</Form.Label>
                                        <Form.Control type='text' value={description} onChange={e => setDescription(e.target.value)} required></Form.Control>
                                    </Form.Group>
                                    <Form.Group controlId='amount'>
                                        <Form.Label>Amount</Form.Label>
                                        <Form.Control type='number' value={amount} onChange={e => setAmount(e.target.value)} required></Form.Control>
                                    </Form.Group>
                                    <Button type='submit'>Update</Button>
                                    
                                </Form>
                            </Card.Body>
                        </Card>
                 
                    </div>
                
                </div>
            </div>
            
            
        
        </>
    )
}