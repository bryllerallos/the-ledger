import { Doughnut } from 'react-chartjs-2';
import UserContext from '../UserContext';
import {useContext} from 'react'



export default function DoughnutChart ({income, expense}) {

    
    const data ={
        labels :['Income', 'Expense'],
        datasets: [{
            data: [income, expense],
            backgroundColor: ['#76E1D4', '#FFA900'],
            borderColor: [ 
            '#262626',
            '#262626'
            
            ]
          
        }]
    }

    
    return(
        <Doughnut data ={data} redraw={false}/>
    )
}