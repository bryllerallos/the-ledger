import React, {useState, useContext} from 'react';
import Link from 'next/link';
import * as FaIcons from 'react-icons/fa';
import * as BsIcons from 'react-icons/bs';
import * as FiIcons from 'react-icons/fi';
import * as AiIcons from 'react-icons/ai';
import * as ImIcons from 'react-icons/im';
import * as IoIcons from 'react-icons/io';
import * as GoIcons from 'react-icons/go';
import {IconContext} from 'react-icons';
import UserContext from '../UserContext';
import {Form, Modal, Button} from 'react-bootstrap';




const Navbar = () => {
    const {user, unsetUser, balance} = useContext(UserContext)
   
    

    const [sidebar, setSidebar] = useState(false)

    const showSidebar = () => setSidebar(!sidebar)

    let name = 'The Ledger'

    const [showModal, setShowModal] = useState(false)

    const [currentPassword, setCurrentPassword] = useState('')
    const [newPassword, setNewPassword] = useState('')
    const [newPassword1, setNewPassword1] = useState('')

    const handleClose = () => setShowModal(false)


    const handleModal = () => {
            
        setShowModal(true)

    }


    const handleUpdate = e => {
        e.preventDefault()


        const complete  = (currentPassword !== '' && newPassword !== '' && newPassword1 !== '') && (newPassword === newPassword1)

        if(complete){

            fetch(`http://localhost:4000/user/${user.id}`, {
                method: 'get',
                headers: {
                  Authorization: `Bearer ${localStorage.getItem('googleToken')}`
                }
                
              })
              .then(res => res.json())
              .then(data => {
                  console.log(data)
              })
        }else{
            alert('incomplete')
        }
    }

    

    return(
        <>

                <Modal show={showModal}
                    onHide={handleClose}
                    size="sm"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered
                >
                    <Modal.Header closeButton>
                        <Modal.Title className='text-center'>Change Password</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                    <Form autoComplete='off'>
                                        <Form.Group controlId='oldPassword'>
                                            <Form.Label>Current Password</Form.Label>
                                            <Form.Control className='badge-pill' type='password' value={currentPassword} onChange={e => setCurrentPassword(e.target.value)} required></Form.Control>
                                        </Form.Group>
                                       
                                        <Form.Group controlId='newPassword'>
                                            <Form.Label>New Password</Form.Label>
                                            <Form.Control className='badge-pill' type='password' value={newPassword}  onChange={e => setNewPassword(e.target.value)} required></Form.Control>
                                        </Form.Group>
                                        <Form.Group controlId='newPassword1'>
                                            <Form.Label>Confirm New Password</Form.Label>
                                            <Form.Control className='badge-pill' type='password' value={newPassword1}  onChange={e => setNewPassword1(e.target.value)}></Form.Control>
                                        </Form.Group>
                                        <Button className='badge-pill btn-block btn-outline btn-warning' onClick={handleUpdate}>
                                            Submit
                                        </Button>
                                        <Button className='badge-pill btn-block btn-outline btn-info' onClick={handleClose}>
                                            Cancel
                                        </Button>
                                        
                                    </Form>
                    </Modal.Body>
                </Modal>


        

            <IconContext.Provider value={{color: '#fff'}}>
                <div className="navbar">
                    
                    <span id='span' type='click' className='menu bars'><ImIcons.ImBook size={30} className='icons' onClick={showSidebar}/></span>
                        
                    <h4 id='title' className='name anim-typewriter'>{name}</h4>
                    
                </div>
                <nav className={sidebar ? 'nav-menu active' : 'nav-menu'}>
                    <ul className='nav-menu-items' onClick={showSidebar}>
                    {(user.id !== null)
                                             
                     ?
                     
                        (user.loginType !== 'email') ?

                    <>

                        <li className="navbar-toggle">
                             <IoIcons.IoIosArrowBack className='back' size={35}/>
                        </li>
                     

                        <li className='nav-text'>
                            <AiIcons.AiFillHome size={25}/>
                            <Link href='/home'>
                               <a role='button'>Home</a>
                            </Link>
                        </li>

                        <li className='nav-text'>
                            <FaIcons.FaHistory size={25}/>
                            <Link href='/presentation'>
                               <a role='button'>Record Analysis</a>
                            </Link>
                        </li>


                        <li className='nav-text'>
                            <FiIcons.FiLogIn size={25}/>
                            <Link href='/logout'>
                               <a role='button' onClick={unsetUser}>Logout</a>
                            </Link>
                        </li>

                     </>   

                        :


                        <>
                       
                        <li className="navbar-toggle">
                             <IoIcons.IoIosArrowBack className='back' size={35}/>
                        </li>
                     
                        <li className='nav-text'>
                            <AiIcons.AiFillHome size={25}/>
                            <Link href='/home'>
                               <a role='button'>Home</a>
                            </Link>
                        </li>

                        <li className='nav-text'>
                            <FaIcons.FaHistory size={25}/>
                            <Link href='/presentation'>
                               <a role='button'>Record Analysis</a>
                            </Link>
                        </li>

                        {/* <li className='nav-text'>
                            <GoIcons.GoGear size={25}/>
                            <Link href='/home'>
                               <a role='button' type='button' onClick={handleModal}>Change Password</a>
                            </Link>
                            
                        </li> */}

                        <li className='nav-text'>
                            <FiIcons.FiLogIn size={25}/>
                            <Link href='/logout'>
                               <a role='button' onClick={unsetUser}>Logout</a>
                            </Link>
                        </li>
                        </>

                        :

                        <>
                        <li className="navbar-toggle">
                             <IoIcons.IoIosArrowBack className='back' size={35}/>
                        </li>

                        <li className='nav-text'>
                            <FiIcons.FiLogIn size={25}/>
                            <Link href='/login'>
                               <a role='button'>Login</a>
                            </Link>
                        </li>

                        <li className='nav-text'>
                            <FiIcons.FiLogIn size={25}/>
                            <Link href='/register'>
                               <a role='button'>Register</a>
                            </Link>
                        </li>
                        </>
                    }

                    </ul>
                </nav>
            </IconContext.Provider>
        </>
    )
}

export default Navbar