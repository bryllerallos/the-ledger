import React from 'react';

const Pagination = ({recordsPerPage, totalRecords, paginate}) => {

    const pageNumbers = [];

    for(let i =1; i <= Math.ceil(totalRecords / recordsPerPage); i++){
        pageNumbers.push(i);
    }

    // console.log(pageNumbers)

    return(
        <nav className='text-center'>
            <ul className='pagination mt-2 text-center '>
                <strong className='pt-2 mr-1'>Page</strong>
                {pageNumbers.map(number =>(
                    <li key={number} className='page-item'>
                    
                        <button onClick={()=> paginate(number)} className='m-1 buttons badge-pill btn-outline-primary bg-light'>
                         {number}
                        </button>
                    </li>
                ))}
            </ul>
        </nav>
    )
}

export default Pagination;